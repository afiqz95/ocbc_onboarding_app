import React, { Component } from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import history from './History';

//Pages that DCI design
import PermissionOnetoken from './components/feature/permission/Permission-Onetoken';
import Permission from './components/feature/permission/Permission'
import QrSetup from './components/feature/qr-setup/Qr-Setup'
import QrSetupSetting from './components/feature/qr-setup/Qr-Setup-Setting'
import AccountLimit from './components/feature/account-limit/Account-Limit'
import AccountLimitSetting from './components/feature/account-limit/Account-Limit-Setting'
import QrSuccess from './components/feature/qr-success/Qr-Success'
import QrSuccessBlank from './components/feature/qr-success/Qr-Success-Blank'
import QrSuccessNoCard from './components/feature/qr-success/Qr-Success-NoCard'
import QrFail from './components/feature/qr-fail/Qr-Fail'
import Deactivate from './components/feature/deactivate/Deactivate-Menu'



export default class Routes extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route path="/" exact component={PermissionOnetoken} />
                    <Route path="/Permission" component={Permission} />
                    <Route path="/QrSetup" component={QrSetup} />
                    <Route path="/QrSetupSetting" component={QrSetupSetting} />
                    <Route path="/AccountLimit" component={AccountLimit} />
                    <Route path="/AccountLimitSetting" component={AccountLimitSetting} />
                    <Route path="/QrSuccess" component={QrSuccess} />
                    <Route path="/QrSuccessBlank" component={QrSuccessBlank} />
                    <Route path="/QrSuccessNoCard" component={QrSuccessNoCard} />
                    <Route path="/QrFail" component={QrFail} />
                    <Route path="/Deactivate" component={Deactivate} />
                </Switch>
            </Router>
        )
    }
}