import React, { Component } from 'react'

interface IModalProps {
    show?:boolean
}

export class Modal extends Component <IModalProps>{

    render() {
        const hideShowModal = this.props.show ? "modal display-block" : "display-none"
        const insideModal = {
            width: "100%",
            height: "fit-content",
            background: "white",
            padding: "16px",
            margin: "auto",
            borderRadius: "18px",
            marginTop:"50px"
        }

        if(this.props.show){
            document.body.style.overflow = 'hidden'
        }else{
            document.body.style.overflow = 'unset'
        }

        return (
            <div className={hideShowModal}>
                <div style={insideModal}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Modal
