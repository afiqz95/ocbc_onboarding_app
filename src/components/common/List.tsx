import React, { Component } from 'react'

interface IListProps{
    click(value:number | null):void,
    value:number
}



export class List extends Component<IListProps> {

    clickEvent = () => {
        this.props.click(this.props.value);
    }

    render() {
        const list = {
            borderBottom:"1px solid #DCE2E5",
            width:"100%",
            display:"flex",
            marginBottom:'16px'
        }
        return (
            <div style={list} onClick={this.clickEvent}>
                {this.props.children}
            </div>
        )
    }
}

export default List
