import React, { Component } from 'react'

interface IButtonProps {
    click():void,
    colored?:boolean
}

export class Button extends Component<IButtonProps> {

    clickEvent = () => {
        this.props.click();
    }

    render() {
        const button = {
            background: this.props.colored ? "linear-gradient(90deg, rgba(21,173,175,1) 0%, rgba(33,210,163,1) 100%)" : "rgb(216,216,216)",
            height: "60px",
            borderRadius: "8px",
            margin:"8px 0px"
        }

        return (
            <div style={button} onClick={this.clickEvent}>
                {this.props.children}
            </div>
        )
    }
}

export default Button
