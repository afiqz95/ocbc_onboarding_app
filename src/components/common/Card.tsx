import React, { Component } from 'react'

interface ICardProps {
    click():void
}

export class Card extends Component<ICardProps>{

    clickEvent = () => {
        this.props.click();
    }

    render() {
        const cardFrame = {
            width:"100%",
            padding:"14px",
            background:"white",
            display:"flex",
            borderRadius:"14px",
            boxShadow:"0px 2px 12px 2px rgba(0,0,0,0.08)"
        }

        
        return (
            <div style={cardFrame} onClick={this.clickEvent}>
                {this.props.children}
            </div>
        )
    }
}

export default Card
