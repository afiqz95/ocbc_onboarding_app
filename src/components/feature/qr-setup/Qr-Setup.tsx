import React, { Component } from "react";
import "./Qr-Setup.scss";
import { MdClose } from "react-icons/md";
import { IoIosArrowBack } from "react-icons/io";
import check from "../../../assets/image/radio_button_check.svg";
import unchecked from "../../../assets/image/radio_button.svg";
import easi from "../../../assets/image/Easi.svg";
import threesixty from "../../../assets/image/360.svg";
import threesixtyfive from "../../../assets/image/365.svg";
import cardTitanium from "../../../assets/image/cardType.png";
import history from "../../../History";
import List from "../../common/List";
import Button from "../../common/Button";
import Modal from "../../common/Modal";

const radioButton = {
  done: check,
  undone: unchecked,
};

export class QrSetup extends Component {
  state = {
    counter: 0,
    listone: false,
    listtwo: false,
    listthree: false,
    listfour: false,
    other: false,
  };

  gotoAccountLimit = () => {
    history.push("/AccountLimit");
  };

  listone = () => {
    let { listone } = this.state;
    this.setState({
      listone: !listone,
    });
  };
  listtwo = () => {
    let { listtwo } = this.state;
    this.setState({
      listtwo: !listtwo,
    });
  };
  listthree = () => {
    let { listthree } = this.state;
    this.setState({
      listthree: !listthree,
    });
  };
  listfour = () => {
    let { listfour } = this.state;
    this.setState({
      listfour: !listfour,
    });
  };
  closeOther = () => {
    let { other } = this.state;
    this.setState({ other: !other });
  };

  getListOne = () => (this.state.listone ? "done" : "undone");
  getListTwo = () => (this.state.listtwo ? "done" : "undone");
  getListThree = () => (this.state.listthree ? "done" : "undone");
  getListFour = () => (this.state.listfour ? "done" : "undone");

  render() {
    const listOneImage = this.getListOne();
    const listTwoImage = this.getListTwo();
    const listThreeImage = this.getListThree();
    const listFourImage = this.getListFour();

    return (
      <div>
        <div className="App-Header">
          <IoIosArrowBack
            size={30}
            className="Icon-Left"
            onClick={() => {
              history.goBack();
            }}
          ></IoIosArrowBack>
          <MdClose
            size={30}
            className="Icon-Right"
            onClick={() => {
              let { other } = this.state;
              this.setState({
                other: !other,
              });
            }}
          ></MdClose>
          <h5 className="text-center header-title">QR Setup</h5>
        </div>
        <div className="App-Body" style={{ padding: "60px 20px" }}>
          <div style={{}}>
            <h2>Accounts</h2>
            <p className="qr-subtitle">Select an account to receive and spend with</p>
            <List value={0} click={() => this.listone()}>
              <div className="col-1" style={{ paddingLeft: "0px" }}>
                <img src={easi} alt="easi-account" style={{ height: "54px" }} />
              </div>
              <div className="col-2" style={{ paddingLeft: "22px", paddingBottom: "22px" }}>
                <h4 className="account-name">Easi Save</h4>
                <p className="account-number">123-123456-12</p>
                <h3 className="account-amount">RM10,000.00</h3>
              </div>
              <div className="col-3">
                <img src={radioButton[listOneImage]} alt="radio-button" />
              </div>
            </List>
            <List value={0} click={() => this.listtwo()}>
              <div className="col-1" style={{ paddingLeft: "0px" }}>
                <img src={threesixty} alt="360-account" style={{ height: "54px" }} />
              </div>
              <div className="col-2" style={{ paddingLeft: "22px", paddingBottom: "22px" }}>
                <h4 className="account-name">360 Account</h4>
                <p className="account-number">123-123456-12</p>
                <h3 className="account-amount">RM10,000.00</h3>
              </div>
              <div className="col-3">
                <img src={radioButton[listTwoImage]} alt="radio-button" />
              </div>
            </List>
            <h2 style={{ marginTop: "20px" }}>Cards</h2>
            <p className="qr-subtitle">Select a card merchant payments (optional)</p>
            <List value={0} click={() => this.listthree()}>
              <div className="col-1" style={{ paddingLeft: "0px" }}>
                <img
                  src={cardTitanium}
                  alt="titanium-card"
                  style={{ height: "80px", marginLeft: "-18px", marginTop: "-22px" }}
                />
              </div>
              <div
                className="col-2"
                style={{ paddingLeft: "22px", paddingBottom: "22px", paddingTop: "16px" }}
              >
                <h4 className="card-name">OCBC Titanium</h4>
                <p className="card-number">4589 1234 8923 2334</p>
              </div>
              <div className="col-3">
                <img src={radioButton[listThreeImage]} alt="radio-button" />
              </div>
            </List>
            <List value={0} click={() => this.listfour()}>
              <div className="col-1" style={{ paddingLeft: "0px" }}>
                <img
                  src={threesixtyfive}
                  alt="365-card"
                  style={{ height: "32px", marginLeft: "4px" }}
                />
              </div>
              <div
                className="col-2"
                style={{ paddingLeft: "22px", paddingBottom: "22px", paddingTop: "16px" }}
              >
                <h4 className="card-name">OCBC 365</h4>
                <p className="card-number">1302 1321 6789 5678</p>
              </div>
              <div className="col-3">
                <img src={radioButton[listFourImage]} alt="radio-button" />
              </div>
            </List>
          </div>
        </div>
        <div className="App-Footer">
          {this.state.listone || this.state.listtwo ? (
            <Button
              colored
              click={() => {
                this.gotoAccountLimit();
              }}
            >
              <h4 className="button-title">Next</h4>
            </Button>
          ) : (
            <Button
              click={() => {
                console.log("unchecked");
              }}
            >
              <h4 className="button-title">Next</h4>
            </Button>
          )}
        </div>
        <Modal show={this.state.other}>
          <MdClose
            size={30}
            className="Icon-Right"
            onClick={() => {
              this.closeOther();
            }}
          ></MdClose>
          <h4 className="popup-body">
            You are about to leave this application. If you leave, all your preferences will be left
            unsaved.
          </h4>
          <p className="popup-question">Do you wish to continue?</p>
          <Button
            colored
            click={() => {
              this.closeOther();
            }}
          >
            <h4 className="button-title">Continue</h4>
          </Button>
          <div
            className="popup-button"
            onClick={() => {
              this.closeOther();
            }}
          >
            <h4 className="popup-button-title">Cancel</h4>
          </div>
        </Modal>
      </div>
    );
  }
}

export default QrSetup;
