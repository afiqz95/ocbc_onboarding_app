import React, { Component } from 'react'
import './Qr-Fail.scss'
import { IoIosArrowBack } from 'react-icons/io'
import fail from '../../../assets/image/fail.svg'
import history from '../../../History'


export class QrFail extends Component {
    render() {
        return (
            <div>
                <div className="App-Header">
                    <IoIosArrowBack size={30} className="Icon-Left" onClick={() => {
                        history.goBack();
                    }} />
                </div>
                <div className="App-Body">
                    <img src={fail} alt="success-logo" style={{ marginRight: 'auto', marginLeft: 'auto', width: '100%', height: '80px', marginTop: '60px' }} />
                    <h2 style={{ textAlign: 'center', color: 'darkslategray', marginTop: '14px' }}>Sorry, we are unable to activate your DuitNow QR</h2>
                    <div style={{ textAlign: 'center', marginTop: '22px' }}>
                        <p><span><a href="https://www.ocbc.com.my/eforms/personalbankingcustomerfeedback" style={{ color: "#2D5EFD" }}>Get in touch</a></span> with us for assistance or try again later.</p>
                    </div>
                </div>
                <div className="App-Footer">
                    <div className="popup-button" onClick={() =>{
                        history.push("/Permission")
                    }}>
                        <h4 className="popup-button-title">Back to Home</h4>
                    </div>
                </div>
            </div>
        )
    }
}

export default QrFail
