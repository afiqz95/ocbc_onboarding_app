import React, { Component } from 'react'
import {IoIosArrowBack} from 'react-icons/io'
import success from '../../../assets/image/success.svg'
import history from '../../../History'
import Button from '../../common/Button'

export class QrSuccessBlank extends Component {
    render() {
        return (
            <div>
                <div className="App-Header">
                    <IoIosArrowBack size={30} className="Icon-Left" onClick={() => {
                        history.goBack();
                    }} />
                    <h5 className="text-center header-title" style={{ marginRight: '20px' }}>DuitNow QR Ready</h5>
                </div>
                <div className="App-Body">
                    <img src={success} alt="success-logo" style={{ marginRight: 'auto', marginLeft: 'auto', width: '100%', height: '80px', marginTop: '60px' }} />
                    <h2 style={{ textAlign: 'center', color: 'darkslategray', marginTop: '14px' }}>Your DuitNow QR is now ready for use</h2>
                </div>
                <div className="App-Footer">
                    <Button click={()=>{
                        history.push("/Deactivate")
                    }} colored>
                        <h4 className="button-title">Done</h4>
                    </Button>
                </div>
            </div>
        )
    }
}

export default QrSuccessBlank
