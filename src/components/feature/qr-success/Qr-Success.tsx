import React, { Component } from 'react'
import './Qr-Success.scss'
import success from '../../../assets/image/success.svg'
import faceid from '../../../assets/image/face-id_gray.svg'
import done from '../../../assets/image/radio_button_check.svg'
import { IoIosArrowBack } from 'react-icons/io'
import history from '../../../History'
import Card from '../../common/Card'
import Button from '../../common/Button'

export class QrSuccess extends Component {
    render() {

        const style = {
            width: '100%'
        }

        return (
            <div>
                <div className="App-Header">
                    <IoIosArrowBack size={30} className="Icon-Left" onClick={() => {
                        history.goBack();
                    }} />
                    <h5 className="text-center header-title" style={{ marginRight: '20px' }}>DuitNow QR Ready</h5>
                </div>
                <div className="App-Body">
                    <img src={success} alt="success-logo" style={{ marginRight: 'auto', marginLeft: 'auto', width: '100%', height: '80px', marginTop: '60px' }} />
                    <h2 style={{ textAlign: 'center', color: 'darkslategray', marginTop: '14px' }}>You have successfully activated DuitNow QR</h2>
                    <div className="row">
                        <div className="box-info-left">
                            <p className="title-1">Preferred account</p>
                            <h4 className="title-2">Easi Save</h4>
                            <p className="title-3">123-123456-12</p>
                        </div>
                        <div className="box-info-right">
                            <p className="title-1">Preferred Credit Card</p>
                            <h4 className="title-2">OCBC Titanium</h4>
                            <p className="title-3">4589 1234 8923 2334</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="box-info-left">
                            <p className="title-1">QR Daily limit</p>
                            <h4>RM30,000</h4>
                        </div>
                        <div className="box-info-right">
                            <p className="title-1">Cumulative limit</p>
                            <h4>RM250</h4>
                        </div>
                    </div>
                    <div style={{ marginTop: '30px' }}>
                        <Card click={() =>{}}>
                            <div className="col-1" style={{ paddingLeft: '8px' }}>
                                <img src={faceid} alt="face-id" />
                            </div>
                            <div className="col-2" style={{ padding: '12px', width: '80%' }}>
                                <p style={{ fontSize: '14px',textAlign:'center' }}>OneLook has been activated</p>
                            </div>
                            <div className="col-3" style={{ width: '12%', paddingTop: '8px' }}>
                                <img src={done} alt="check-button" />
                            </div>
                        </Card>
                    </div>
                </div>
                <div className="App-Footer">
                    <Button colored click={() => {history.push('/QrFail')}}>
                        <h4 className="button-title">Done</h4>
                    </Button>
                </div>
            </div>
        )
    }
}

export default QrSuccess
