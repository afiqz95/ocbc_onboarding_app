import React, { Component } from "react";
import { MdClose } from "react-icons/md";
import "./Permission.scss";
import camera from "../../../assets/image/camera.svg";
import faceId from "../../../assets/image/face-id_gray.svg";
import uncheck from "../../../assets/image/radio_button.svg";
import check from "../../../assets/image/radio_button_check.svg";
import logo from "../../../assets/image/hand_qr.svg";
import onetoken from "../../../assets/image/OneToken.svg";
import history from "../../../History";
import Card from "../../common/Card";
import Button from "../../common/Button";
import Modal from "../../common/Modal";

const radioButton = {
  done: check,
  undone: uncheck,
};

export class PermissionOnetoken extends Component {
  state = {
    camera: false,
    security: false,
    token: false,
    modal: false,
    toggle: false,
    other: false,
  };

  camera = () => {
    let { camera } = this.state;
    this.setState({
      camera: !camera,
    });
    console.log(this.state.camera);
  };

  openModal = () => {
    if (!this.state.security) {
      this.setState({
        modal: true,
      });
    }
  };

  token = () => {
    let { token } = this.state;
    this.setState({
      token: !token,
    });
    console.log(this.state.token);
  };

  continue = () => {
    let { security, modal } = this.state;
    this.setState({
      modal: false,
      security: true,
    });
  };

  closeModal = () => {
    let { modal } = this.state;
    this.setState({ modal: !modal });
  };
  closeOther = () => {
    let { other } = this.state;
    this.setState({ other: !other });
  };

  gotoQrSetup = () => {
    history.push("/QrSetup");
  };

  getCamera = () => (this.state.camera ? "done" : "undone");
  getSecurity = () => (this.state.security ? "done" : "undone");
  getToken = () => (this.state.token ? "done" : "undone");

  render() {
    const imageCamera = this.getCamera();
    const imageSecurity = this.getSecurity();
    const imageToken = this.getToken();

    return (
      <div>
        <div className="App-Body" style={{ paddingBottom: "80px" }}>
          <MdClose
            size={30}
            className="Icon-Right"
            onClick={() => {
              let { other } = this.state;
              this.setState({
                other: !other,
              });
            }}
          />
          <img src={logo} alt="hand_qr" className="hand-logo" />
          <h2 className="text-title">Say hello to DuitNow QR</h2>
          <p className="text-subtitle">With QRPay, you can now make transfers and payments through your phone. Activate the following requirements to enjoy this feature now.</p>

          <div className="center">
            <Card click={() => this.camera()}>
              <div className="col-1">
                <img src={camera} alt="camera" style={{ height: "24px" }} />
              </div>
              <div className="col-2">
                <h4 className="card-title">Allow Camera Permission</h4>
                <p className="card-subtitle">Kindly allow access to your camera app for QR scanning.</p>
              </div>
              <div className="col-3">
                <img src={radioButton[imageCamera]} alt="radio-button" />
              </div>
            </Card>
          </div>
          <div className="center">
            <Card click={() => this.openModal()}>
              <div className="col-1">
                <img src={faceId} alt="camera" style={{ height: "24px" }} />
              </div>
              <div className="col-2">
                <h4 className="card-title">Activate OCBC OneLook</h4>
                <p className="card-subtitle">With OneLook you can enjoy secured verification.</p>
              </div>
              <div className="col-3">
                <img src={radioButton[imageSecurity]} alt="radio-button" />
              </div>
            </Card>
          </div>
          <div className="center">
            <Card click={() => this.token()}>
              <div className="col-1">
                <img src={onetoken} alt="camera" style={{ height: "24px" }} />
              </div>
              <div className="col-2">
                <h4 className="card-title">OCBC OneToken activation</h4>
                <p className="card-subtitle">Activate OneToken for greater convenience and security.</p>
              </div>
              <div className="col-3">
                <img src={radioButton[imageToken]} alt="radio-button" />
              </div>
            </Card>
          </div>
          <p style={{ fontSize: "11px", marginTop: "18px", textAlign: "center" }}>
            Tap Continue to setup, if you read and agree to{" "}
            <span style={{ textDecoration: "underline" }}>
              <a href="https://www.ocbc.com.my" target="_blank">
                terms of service
              </a>
            </span>
          </p>
        </div>
        <div className="App-Footer">
          {this.state.camera && this.state.security && this.state.token ? (
            <Button
              colored
              click={() => {
                this.gotoQrSetup();
              }}
            >
              <h4 className="button-title">Continue to setup</h4>
            </Button>
          ) : (
            <Button
              click={() => {
                console.log("unchecked");
              }}
            >
              <h4 className="button-title">Continue to setup</h4>
            </Button>
          )}
        </div>
        <Modal show={this.state.modal}>
          <MdClose
            size={30}
            className="Icon-Right"
            onClick={() => {
              this.closeModal();
            }}
          ></MdClose>
          <h4 className="popup-body">For your security, you can only activate OCBC OneTouch and DuitNow QRPay on one device. If activated here, your previous device features will be deactivated.</h4>
          <p className="popup-question">Do you wish to continue?</p>
          <Button
            colored
            click={() => {
              this.continue();
            }}
          >
            <h4 className="button-title">Continue</h4>
          </Button>
          <div
            className="popup-button"
            onClick={() => {
              this.closeModal();
            }}
          >
            <h4 className="popup-button-title">Cancel</h4>
          </div>
        </Modal>
        <Modal show={this.state.other}>
          <MdClose
            size={30}
            className="Icon-Right"
            onClick={() => {
              this.closeOther();
            }}
          ></MdClose>
          <h4 className="popup-body">You are about to leave this application. If you leave, all your preferences will be left unsaved.</h4>
          <p className="popup-question">Do you wish to continue?</p>
          <Button
            colored
            click={() => {
              this.closeOther();
            }}
          >
            <h4 className="button-title">Continue</h4>
          </Button>
          <div
            className="popup-button"
            onClick={() => {
              this.closeOther();
            }}
          >
            <h4 className="popup-button-title">Cancel</h4>
          </div>
        </Modal>
      </div>
    );
  }
}

export default PermissionOnetoken;
