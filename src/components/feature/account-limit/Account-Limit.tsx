import React, { Component } from "react";
import history from "../../../History";
import "./Account-Limit.scss";
import { IoIosArrowBack } from "react-icons/io";
import { MdClose } from "react-icons/md";
import { MdKeyboardArrowDown } from "react-icons/md";
import questionMark from "../../../assets/image/question.svg";
import check from "../../../assets/image/radio_button_check.svg";
import unchecked from "../../../assets/image/radio_button.svg";
import chevron from "../../../assets/image/chevron.svg";
import List from "../../common/List";
import Button from "../../common/Button";
import Drawer from "react-drag-drawer";
import Modal from "../../common/Modal";

let daily = [
  { label: "30,000", value: 30000 },
  { label: "20,000", value: 20000 },
  { label: "10,000", value: 10000 },
  { label: "5,000", value: 5000 },
  { label: "3,000", value: 3000 },
  { label: "1,000", value: 1000 },
  { label: "500", value: 500 },
];

let cumulative = [
  { label: "250", value: 250 },
  { label: "200", value: 200 },
  { label: "150", value: 150 },
  { label: "100", value: 100 },
  { label: "50", value: 50 },
];

export class AccountLimit extends Component {
  state = {
    infoDaily: false,
    infoCumulative: false,
    openDaily: false,
    openCumulative: false,
    dailyAmount: 0,
    cumulativeAmount: 0,
    other: false,
  };

  infoDaily = () => {
    let { infoDaily } = this.state;
    this.setState({
      infoDaily: !infoDaily,
    });
  };

  infoCumulative = () => {
    let { infoCumulative } = this.state;
    this.setState({
      infoCumulative: !infoCumulative,
    });
  };

  openDailyDrawer = () => {
    console.log("daily");
    let { openDaily } = this.state;
    this.setState({
      openDaily: !openDaily,
    });
  };

  openCumulativeDrawer = () => {
    console.log("cumulative");
    let { openCumulative } = this.state;
    this.setState({
      openCumulative: !openCumulative,
    });
  };

  closeOther = () => {
    let { other } = this.state;
    this.setState({ other: !other });
  };

  renderDailyItems() {
    return daily.map((x) => {
      return (
        <List
          key={x.value}
          value={x.value}
          click={(item: any) => {
            this.setState({
              dailyAmount: item,
              openDaily: false,
            });
          }}
        >
          <div className="list-modal-left">
            <p>RM{x.label}</p>
          </div>
          <div className="list-modal-right">
            <img src={this.state.dailyAmount == x.value ? check : unchecked} alt="radio-button" />
          </div>
        </List>
      );
    });
  }
  renderCumulativeItems() {
    return cumulative.map((x) => {
      return (
        <List
          key={x.value}
          value={x.value}
          click={(item: any) => {
            this.setState({
              cumulativeAmount: item,
              openCumulative: false,
            });
          }}
        >
          <div className="list-modal-left">
            <p>RM{x.label}</p>
          </div>
          <div className="list-modal-right">
            <img
              src={this.state.cumulativeAmount == x.value ? check : unchecked}
              alt="radio-button"
            />
          </div>
        </List>
      );
    });
  }
  render() {
    return (
      <div>
        <div className="App-Header">
          <IoIosArrowBack
            size={30}
            className="Icon-Left"
            onClick={() => {
              history.goBack();
            }}
          ></IoIosArrowBack>
          <MdClose
            size={30}
            className="Icon-Right"
            onClick={() => {
              let { other } = this.state;
              this.setState({
                other: !other,
              });
            }}
          ></MdClose>
        </div>
        <div className="App-Body" style={{ paddingBottom: "110px" }}>
          <h2 style={{ marginTop: "70px" }}>DuitNow QR</h2>

          <p className="select-title" style={{ marginTop: "60px" }}>
            QR Daily limit
          </p>
          <List value={0} click={() => this.openDailyDrawer()}>
            <div style={{ width: "92%" }}>
              <h4 className="select-font">RM{this.state.dailyAmount}</h4>
            </div>
            <div style={{ width: "8%" }}>
              <MdKeyboardArrowDown size={25} />
            </div>
          </List>
          <div
            style={{ display: "flex" }}
            onClick={() => {
              this.infoDaily();
            }}
          >
            <p style={{ fontSize: "15px" }}>What is 'QR Daily limit'?</p>
            <img src={questionMark} style={{ marginLeft: "4px" }}></img>
          </div>
          {this.state.infoDaily == true ? (
            <div className="info">
              <p className="info-message">
                <b>QR Daily limit</b> QR Daily limit refers to the total amount you can use with
                DuitNow QRPay within a single day. This limit will be reset on a daily basis.
              </p>
            </div>
          ) : (
            <></>
          )}
          <p className="select-title">Cumulative limit</p>
          <List value={0} click={() => this.openCumulativeDrawer()}>
            <div style={{ width: "92%" }}>
              <h4 className="select-font">RM{this.state.cumulativeAmount}</h4>
            </div>
            <div style={{ width: "8%" }}>
              <MdKeyboardArrowDown size={25} />
            </div>
          </List>
          <div
            style={{ display: "flex" }}
            onClick={() => {
              this.infoCumulative();
            }}
          >
            <p style={{ fontSize: "15px" }}>What is 'Cumulative limit'?</p>
            <img src={questionMark} style={{ marginLeft: "4px" }}></img>
          </div>
          {this.state.infoCumulative == true ? (
            <div className="info">
              <p className="info-message">
                <b>Cumulative limit</b> refers to the amount you can use before you are required to
                authenticate yourself.
                <br />
                <br />
                Your cumulative limit amount cannot be higher than your daily limit.
              </p>
            </div>
          ) : (
            <></>
          )}
        </div>
        <div className="App-Footer">
          <Button
            colored
            click={() => {
              history.push("/QrSuccess");
            }}
          >
            <h4 className="button-title">Next</h4>
          </Button>
        </div>
        <Drawer
          modalElementClass="drawer-modal"
          open={this.state.openDaily}
          onRequestClose={this.openDailyDrawer}
        >
          <img src={chevron} alt="arrow-up" className="chevron-icon" />
          <div>
            <div className="header">
              <h2>QR Daily limit</h2>
            </div>
            <div className="inside">{this.renderDailyItems()}</div>
          </div>
        </Drawer>
        <Drawer
          modalElementClass="drawer-modal"
          open={this.state.openCumulative}
          onRequestClose={this.openCumulativeDrawer}
        >
          <img src={chevron} alt="arrow-up" className="chevron-icon" />
          <div>
            <div className="header">
              <h2>Cumulative limit</h2>
            </div>
            <div className="inside">{this.renderCumulativeItems()}</div>
          </div>
        </Drawer>
        <Modal show={this.state.other}>
          <MdClose
            size={30}
            className="Icon-Right"
            onClick={() => {
              this.closeOther();
            }}
          ></MdClose>
          <h4 className="popup-body">
            You are about to leave this application. If you leave, all your preferences will be left
            unsaved.
          </h4>
          <p className="popup-question">Do you wish to continue?</p>
          <Button
            colored
            click={() => {
              this.closeOther();
            }}
          >
            <h4 className="button-title">Continue</h4>
          </Button>
          <div
            className="popup-button"
            onClick={() => {
              this.closeOther();
            }}
          >
            <h4 className="popup-button-title">Cancel</h4>
          </div>
        </Modal>
      </div>
    );
  }
}

export default AccountLimit;
