import React from 'react'
import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import { withStyles, Theme, createStyles } from '@material-ui/core/styles';
import Drawer from "react-drag-drawer";
import arrow from '../../../assets/image/right-arrow.svg';
import exclamation from "../../../assets/image/exclamation.svg";
import menu from '../../../assets/image/menu.svg';
import close from '../../../assets/image/close.svg';
import './Deactivate.scss';
import history from '../../../History';

interface Styles extends Partial<Record<SwitchClassKey, string>> {
    focusVisible?: string;
}

interface Props extends SwitchProps {
    classes: Styles;
}

const SwitchBtn = withStyles((theme: Theme) =>
    createStyles({
        root: {
            width: 55,
            height: 34,
            padding: 0,
        },
        switchBase: {
            width: 28,
            height: 28,
            padding: "0px",
            color: "#fff",
            boxShadow: "0px 0px 9px 0px rgba(230,230,230,1)",
            transform: 'translate(3px,3px)',
            '&$checked': {
                transform: 'translate(24px,3px)',
                color: theme.palette.common.white,
                '& + $track': {
                    backgroundColor: '#19B7A7',
                    opacity: 1,
                    border: 'none',
                },
            },
            '&$focusVisible $thumb': {
                color: '#52d869',
                border: '6px solid #fff',
            },
        },
        thumb: {
            width: 28,
            height: 28,
        },
        track: {
            borderRadius: 34 / 2,
            border: `1px solid ${theme.palette.grey[300]}`,
            backgroundColor: theme.palette.grey[50],
            opacity: 1,
            transition: theme.transitions.create(['background-color', 'border']),
        },
        checked: {},
        focusVisible: {},
    }),
)(({ classes, ...props }: Props) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}
        />
    );
});

export default function DeactivateMenu() {

    const [state, setState] = React.useState({
        popup: false,
        switch: true,
        div: true,
        reactivate:false
    });

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {

        // setState({ switch: state.switch, popup: true, div: state.div });
        if (state.switch) {
            setState({ switch: state.switch, popup: true, div: state.div, reactivate: false })
        }
        else {
            setState({ switch:false , popup: false, div:false, reactivate:true })
        }
    }

    const closedPopup = () => {
        setState({ popup: false, switch: state.switch, div: state.div, reactivate:state.reactivate })
    }
    const closedReactivate = () => {
        setState({ popup: false, switch: false, div: false, reactivate:false })
    }

    const hideDiv = () => {
        setState({ popup: false, switch: false, div: false, reactivate:false })
    }
    const showDiv = () => {
        setState({ popup: false, switch: true, div: true, reactivate:false })
    }

    const GoQrSetup = () =>{
        history.push("/QrSetupSetting")
    }
    const GoAccountLimit = () =>{
        history.push("/AccountLimitSetting")
    }


    return (
        <div>
            <div className="App-Header">
                <img className="Icon-Left Icon" src={menu} alt="menu-icon"></img>
                <p className="Header-Title">Manage DuitNow QR</p>
            </div>
            <div className="App-Body">
                <div className="Switch-Card">
                    <div className="Left-Card">
                        <p className="semi-font">DuitNow QR</p>
                        <p className="small-font">QR Payment is deactivated</p>
                    </div>
                    <div className="Right-Card">
                        <SwitchBtn checked={state.switch} onChange={handleChange} name="popup" />
                    </div>
                </div>
                <div className="Border-Line"></div>
                {
                    state.div ? <div>
                        <div className="List-Menu" onClick={GoQrSetup}>
                            <div style={{ width: "95%" }}><p className="semi-font">DuitNow QR default account</p></div>
                            <div style={{ paddingTop: "4px" }}><img style={{ width: "8px" }} src={arrow} alt="arrow"></img></div>
                        </div>
                        <div className="List-Menu" onClick={GoAccountLimit}>
                            <div style={{ width: "95%" }}><p className="semi-font">DuitNow QR transfer limit</p></div>
                            <div style={{ paddingTop: "4px" }}><img style={{ width: "8px" }} src={arrow} alt="arrow"></img></div>
                        </div>
                    </div>
                        :
                        <div></div>
                }
            </div>
            <Drawer
                open={state.popup}
                onRequestClose={closedPopup}
                modalElementClass="pop-modal"
            >
                <div>
                    <div className="Header-Popup">
                        <img src={close} alt="close" onClick={closedPopup}/>
                    </div>
                    <img src={exclamation} alt="exclamation" style={{ marginBottom: "12px" }} />
                    <p style={{ marginBottom: "16px", fontFamily: "OpenSans Regular", color: "#5A707B" }}>Are you sure you would like to deactivate DuitNow QR?</p>
                    <div className="Button-Color" onClick={hideDiv}>Yes</div>
                    <div className="Button-Plain" onClick={closedPopup}>No</div>
                </div>
            </Drawer>
            <Drawer
                open={state.reactivate}
                onRequestClose={closedReactivate}
                modalElementClass="pop-modal"
            >
                <div>
                    <div className="Header-Popup">
                        <img src={close} alt="close" onClick={closedReactivate}/>
                    </div>
                    <img src={exclamation} alt="exclamation" style={{ marginBottom: "12px" }} />
                    <p style={{ marginBottom: "16px", fontFamily: "OpenSans Regular", color: "#5A707B" }}>Are you sure you would like to reactivate DuitNow QR?</p>
                    <div className="Button-Color" onClick={showDiv}>Yes</div>
                    <div className="Button-Plain" onClick={closedReactivate}>No</div>
                </div>
            </Drawer>
        </div>
    )

}


