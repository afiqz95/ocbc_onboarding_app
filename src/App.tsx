import React from 'react';
import Routes from './Routes'
import { HashRouter as Router } from 'react-router-dom'
import './assets/scss/main.scss';

function App() {
  return (
    <Router>
      <div className="App">
        <Routes />
      </div>
    </Router>
  );
}

export default App;
